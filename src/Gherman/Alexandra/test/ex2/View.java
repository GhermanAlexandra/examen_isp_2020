package Gherman.Alexandra.test.ex2;

import javax.swing.*;

import java.awt.event.ActionListener;

import static javax.swing.WindowConstants.EXIT_ON_CLOSE;

public class View {
    public  JFrame frame=new JFrame();
    private JTextField jTextField=new JTextField(50);
    private JButton jButton=new JButton("Convert");

    public View(){
        JPanel panel = new JPanel();
        panel.add(jTextField);
        panel.add(jButton);
        frame.add(panel);
        frame.setSize(800, 900);
        frame.setTitle("Convert");
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

    }
    void convertListener(ActionListener a)
    {
        jButton.addActionListener(a);
    }
    String getText(){
        return jTextField.getText();
    }
}