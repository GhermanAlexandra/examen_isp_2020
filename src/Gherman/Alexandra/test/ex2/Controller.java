package Gherman.Alexandra.test.ex2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Controller {
    private View view;

    public Controller(View view){
        this.view=view;
        view.convertListener(new convertListener());
    }

    public class convertListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            String title=view.getText();
            FileReader fr = null;
            try {
                fr = new FileReader(title);
            } catch (FileNotFoundException ex) {
                ex.printStackTrace();
            }
            BufferedReader br = new BufferedReader(fr);
            String ch = null;
            List<String> tmp = new ArrayList<String>();

            do {
                try {
                    ch = br.readLine();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                tmp.add(ch);
                System.out.println(ch+"<br/>");
            } while (ch != null);

            for(int i=tmp.size()-1;i>=0;i--) {
                System.out.println(tmp.get(i)+"<br/>");
            }
        }

    }
}

